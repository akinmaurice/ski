import * as Constants from "../Constants";
import { Entity } from "./Entity";
import { intersectTwoRects, Rect } from "../Core/Utils";

export class Skier extends Entity {
    assetName = Constants.SKIER_DOWN;

    direction = Constants.SKIER_DIRECTIONS.DOWN;
    speed = Constants.SKIER_STARTING_SPEED;
    skierIsJumping = false;

    constructor(x, y) {
        super(x, y);
    }

    increaseSkierSpeed() {
      this.speed = this.speed + (this.speed * Constants.SPEED_INCREMENT);
    }

    setDirection(direction) {
      const { SKIER_DIRECTION_ASSET } = Constants;
      const skier_direction = SKIER_DIRECTION_ASSET[direction];
      if (skier_direction) {
      this.direction = direction;
      this.updateAsset();
      }
      return;
    }

    updateAsset() {
      if(this.skierIsJumping) {
        const jump_1 = Constants.SKIER_JUMP_1;
        const jump_2 = Constants.SKIER_JUMP_2;
        const jump_3 = Constants.SKIER_JUMP_3;
        const jump_4 = Constants.SKIER_JUMP_4;
        const assetArray = [jump_2, jump_3, jump_4];
        let i = 0;
        this.assetName = jump_1;
        const intervalId = setInterval(() => {
          if (i > 2) {
              clearInterval(intervalId);
              this.skierIsJumping = false;
              this.updateAsset();
          } else {
              this.assetName = assetArray[i];
              i += 1;
          }
      }, 300);
        return;
      } else if(!this.skierIsJumping) {
          this.assetName = Constants.SKIER_DIRECTION_ASSET[Constants.SKIER_DIRECTIONS.DOWN];
      }
        this.assetName = Constants.SKIER_DIRECTION_ASSET[this.direction];
    }


    setSkierJumping(arg) {
      if(arg === true) {
        this.increaseSkierSpeed();
      }
      if(this.skierIsJumping !== arg) {
        this.skierIsJumping = arg;
        this.updateAsset();
      }
    }


    resetJumpingTimeout() {
      setTimeout(() => {
            this.setSkierJumping(false)
          }, Constants.SKIER_JUMPING_TIMEOUT);
  }


    move() {
      if(this.getPosition().y >= 1000) {
          this.increaseSkierSpeed();
      }
        switch(this.direction) {
            case Constants.SKIER_DIRECTIONS.LEFT_DOWN:
                this.moveSkierLeftDown();
                break;
            case Constants.SKIER_DIRECTIONS.DOWN:
                this.moveSkierDown();
                break;
            case Constants.SKIER_DIRECTIONS.RIGHT_DOWN:
                this.moveSkierRightDown();
                break;
        }
    }

    moveSkierLeft() {
        this.x -= Constants.SKIER_STARTING_SPEED;
    }

    moveSkierLeftDown() {
        this.x -= this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
    }

    moveSkierDown() {
        this.y += this.speed;
    }

    moveSkierRightDown() {
        this.x += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Constants.SKIER_DIAGONAL_SPEED_REDUCER;
    }

    moveSkierRight() {
        this.x += Constants.SKIER_STARTING_SPEED;
    }

    moveSkierUp() {
        this.y -= Constants.SKIER_STARTING_SPEED;
    }

    turnLeft() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT) {
            this.moveSkierLeft();
        }
        else {
            this.setDirection(((this.direction - 1 < 0) ? Constants.SKIER_DIRECTIONS.LEFT : this.direction - 1));
        }
    }

    turnRight() {
        if(this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierRight();
        }
        else {
            this.setDirection(this.direction + 1);
        }
    }

    turnUp() {
        if(this.direction === Constants.SKIER_DIRECTIONS.LEFT || this.direction === Constants.SKIER_DIRECTIONS.RIGHT) {
            this.moveSkierUp();
        }
    }

    turnDown() {
        this.setDirection(Constants.SKIER_DIRECTIONS.DOWN);
    }

    getCollisionObstacleAsset(asset) {
      const obstacle = Constants.OBSTACLE_ASSET[asset];
      return (obstacle ? obstacle : null);
    }


    checkIfSkierHitObstacle(obstacleManager, assetManager) {
        const asset = assetManager.getAsset(this.assetName);
        const skierBounds = new Rect(
            this.x - asset.width / 2,
            this.y - asset.height / 2,
            this.x + asset.width / 2,
            this.y - asset.height / 4
        );

        const collision = obstacleManager.getObstacles().find((obstacle) => {
            const obstacleAsset = assetManager.getAsset(obstacle.getAssetName());
            const obstaclePosition = obstacle.getPosition();
            const obstacleBounds = new Rect(
                obstaclePosition.x - obstacleAsset.width / 2,
                obstaclePosition.y - obstacleAsset.height / 2,
                obstaclePosition.x + obstacleAsset.width / 2,
                obstaclePosition.y
            );

            return intersectTwoRects(skierBounds, obstacleBounds);
        });

        if(collision) {
          const obstacle = this.getCollisionObstacleAsset(collision.assetName);
          if(obstacle.skierCanJump) {
            this.setSkierJumping(true);
            return;
          }
          if(this.skierIsJumping && obstacle.skierCanSkip) {
            return;
          } else {
            this.setSkierJumping(false);
            this.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
          }
        }
    };
}