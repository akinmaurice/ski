import * as Constants from "../Constants";
import { Entity } from "./Entity";
import { intersectTwoRects, Rect } from "../Core/Utils";

export class Rhino extends Entity {
    assetName = Constants.RHINO_DOWN;
    speed = Constants.RHINO_SPEED;
    isRunning = true;
    caughtSkier = false;

    constructor(x, y) {
        super(x, y);
    }


    increaseRhinoSpeed() {
      this.speed = this.speed + (this.speed * Constants.SPEED_INCREMENT);
    }

    getFinalScore() {
        const { y } = this.getPosition();
        return Math.ceil(y/100);
    }



    updateAsset() {
      if(!this.isRunning) {
        this.assetName = Constants.RHINO_STOPPED;
      }
      if(this.isRunning) {
        this.assetName = Constants.RHINO_DOWN;
      }
      if(this.caughtSkier) {
        const rhinoEaten1 = Constants.RHINO_EATEN_1;
        const rhinoEaten2 = Constants.RHINO_EATEN_2;
        const rhinoEaten3 = Constants.RHINO_EATEN_3;
        const rhinoEaten4 = Constants.RHINO_EATEN_4;
        const rhinoEaten5 = Constants.RHINO_EATEN_5;
        this.assetName = rhinoEaten3;
      }
    }

    move(skierPosition) {
      if(this.getPosition().y >= 1000) {
        this.increaseRhinoSpeed();
    }
        const { x, y } = skierPosition;
          this.moveRhino(x, y);
    }

    moveRhino(x, y) {
        const { x: rhinoPositionX, y: rhinoPositionY } = this.getPosition();
        const skierPositionX = x;
        const skierPositionY = y;
        const xAxisDistance = skierPositionX - rhinoPositionX;
        const yAxisDistance = skierPositionY - rhinoPositionY;
        const distanceBetweenSkierAndRhino = Math.sqrt((xAxisDistance * xAxisDistance) +
        (yAxisDistance * yAxisDistance));
        if(this.isRunning) {
          this.y += Math.round(this.speed * yAxisDistance / distanceBetweenSkierAndRhino);
          this.x += Math.round(this.speed * xAxisDistance / distanceBetweenSkierAndRhino);
        };
    }

    checkIfRhinoAndSkierPositionsIntercept(skier, assetManager) {
      const asset = assetManager.getAsset(this.assetName);
      const rhinoBounds = new Rect(
        this.x - asset.width / 2,
        this.y - asset.height / 2,
        this.x + asset.width / 2,
        this.y - asset.height / 4
    );
      const skierPosition = skier.getPosition();
      const skierAsset = assetManager.getAsset(skier.getAssetName());
      const skierBounds = new Rect(
        skierPosition.x - skierAsset.width / 2,
        skierPosition.y - skierAsset.height / 2,
        skierPosition.x + skierAsset.width / 2,
        skierPosition.y
    );
    if(skier.assetName === Constants.SKIER_CRASH) {
      this.isRunning = true;
      this.updateAsset();
    }

    if(intersectTwoRects(skierBounds, rhinoBounds)) {
      this.isRunning = false;
      this.caughtSkier = true;
      this.updateAsset();
      skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
      const score = this.getFinalScore();
      if (confirm(`Game Over! You score: ${score}`)) {
        document.location.reload(true);
      }
    };
    }
}