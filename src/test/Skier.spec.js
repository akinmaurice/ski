import 'babel-polyfill';
import * as Constants from '../Constants';
import { Skier } from '../Entities/Skier';


describe('Skier Test', () => {
    let skier;
    beforeEach(() => {
        skier = new Skier(0, 0);
    });

    it('Should Check if Skier Crashes when direction is not in the skier direction asset', () => {
        skier.setDirection(10);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.DOWN);
    });

    it('Should Check if Skier moves left when direction is 1', () => {
        skier.setDirection(1);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT);
    });


    it('Should Check if Skier moves left down when direction is 2', () => {
        skier.setDirection(2);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT_DOWN);
    });


    it('Should Check if Skier moves Down when direction is 3', () => {
        skier.setDirection(3);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.DOWN);
    });


    it('Should Check if Skier moves right down when direction is 4', () => {
        skier.setDirection(4);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT_DOWN);
    });


    it('Should Check if Skier moves right when direction is 5', () => {
        skier.setDirection(5);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.RIGHT);
    });


    it('Should Check if Skier Turns Left After Crash', () => {
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
        skier.turnLeft();
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.LEFT);
    });
});
