import 'babel-polyfill';
import * as Constants from '../Constants';
import { Skier } from '../Entities/Skier';
import { Rhino } from '../Entities/Rhino';


describe('Rhino Test', () => {
    let skier,
        rhino;
    beforeEach(() => {
        skier = new Skier(0, 0);
        rhino = new Rhino(0, -1000);
    });


    it('Should Confirm Rhino Chases Skier when initialized', () => {
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.DOWN);
        expect(rhino.assetName).toBe(Constants.RHINO_DOWN);
    });


    it('Should Confirm Rhino Stops Chasing Skier when Skier Crashes into obstacle', () => {
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
        rhino.isRunning = false;
        rhino.updateAsset();
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.CRASH);
        expect(rhino.assetName).toBe(Constants.RHINO_STOPPED);
    });


    it('Should Confirm Rhino Eats Skier when their positions intercept', () => {
        rhino.isRunning = false;
        rhino.caughtSkier = true;
        rhino.updateAsset();
        skier.setDirection(Constants.SKIER_DIRECTIONS.CRASH);
        expect(skier.direction).toBe(Constants.SKIER_DIRECTIONS.CRASH);
        expect(rhino.assetName).toBe(Constants.RHINO_EATEN_3);
    });
});
