export const GAME_WIDTH = window.innerWidth;
export const GAME_HEIGHT = window.innerHeight;

export const SKIER_CRASH = 'skierCrash';
export const SKIER_LEFT = 'skierLeft';
export const SKIER_LEFTDOWN = 'skierLeftDown';
export const SKIER_DOWN = 'skierDown';
export const SKIER_RIGHTDOWN = 'skierRightDown';
export const SKIER_RIGHT = 'skierRight';
export const SKIER_JUMP = 'skierJump';
export const SKIER_JUMP_1 = 'skierJump1';
export const SKIER_JUMP_2 = 'skierJump2';
export const SKIER_JUMP_3 = 'skierJump3';
export const SKIER_JUMP_4 = 'skierJump4';
export const TREE = 'tree';
export const TREE_CLUSTER = 'treeCluster';
export const ROCK1 = 'rock1';
export const ROCK2 = 'rock2';
export const JUMP_RAMP = 'jump_ramp';
export const RHINO_DOWN = 'rhinoDown';
export const RHINO_EATEN_1 = 'rhinoEaten1';
export const RHINO_EATEN_2 = 'rhinoEaten2';
export const RHINO_EATEN_3 = 'rhinoEaten3';
export const RHINO_EATEN_4 = 'rhinoEaten4';
export const RHINO_EATEN_5 = 'rhinoEaten5';
export const RHINO_STOPPED = 'rhinoStopped';

export const SKIER_STARTING_SPEED = 10;
export const SKIER_DIAGONAL_SPEED_REDUCER = 1.4142;
export const RHINO_SPEED = 9;
export const SKIER_JUMPING_TIMEOUT = 1200;
export const SPEED_INCREMENT = 0.0001;

export const ASSETS = {
    [SKIER_CRASH]: 'img/skier_crash.png',
    [SKIER_LEFT]: 'img/skier_left.png',
    [SKIER_LEFTDOWN]: 'img/skier_left_down.png',
    [SKIER_DOWN]: 'img/skier_down.png',
    [SKIER_RIGHTDOWN]: 'img/skier_right_down.png',
    [SKIER_RIGHT]: 'img/skier_right.png',
    [SKIER_JUMP]: 'img/skier_jump_5.png',
    [SKIER_JUMP_1]: 'img/skier_jump_1.png',
    [SKIER_JUMP_2]: 'img/skier_jump_2.png',
    [SKIER_JUMP_3]: 'img/skier_jump_3.png',
    [SKIER_JUMP_4]: 'img/skier_jump_4.png',
    [TREE]: 'img/tree_1.png',
    [TREE_CLUSTER]: 'img/tree_cluster.png',
    [ROCK1]: 'img/rock_1.png',
    [ROCK2]: 'img/rock_2.png',
    [JUMP_RAMP]: 'img/jump_ramp.png',
    [RHINO_DOWN]: 'img/rhino_run_left.png',
    [RHINO_EATEN_1]: 'img/rhino_lift_mouth_open.png',
    [RHINO_EATEN_2]: 'img/rhino_lift_eat_1.png',
    [RHINO_EATEN_3]: 'img/rhino_lift_eat_2.png',
    [RHINO_EATEN_4]: 'img/rhino_lift_eat_3.png',
    [RHINO_EATEN_5]: 'img/rhino_lift_eat_4.png',
    [RHINO_STOPPED]: 'img/rhino_default.png'
};

export const SKIER_DIRECTIONS = {
    CRASH: 0,
    LEFT: 1,
    LEFT_DOWN: 2,
    DOWN: 3,
    RIGHT_DOWN: 4,
    RIGHT: 5
};

export const SKIER_DIRECTION_ASSET = {
    [SKIER_DIRECTIONS.CRASH]: SKIER_CRASH,
    [SKIER_DIRECTIONS.LEFT]: SKIER_LEFT,
    [SKIER_DIRECTIONS.LEFT_DOWN]: SKIER_LEFTDOWN,
    [SKIER_DIRECTIONS.DOWN]: SKIER_DOWN,
    [SKIER_DIRECTIONS.RIGHT_DOWN]: SKIER_RIGHTDOWN,
    [SKIER_DIRECTIONS.RIGHT]: SKIER_RIGHT,
    [SKIER_DIRECTIONS.JUMP]: SKIER_JUMP,
    [SKIER_DIRECTIONS.JUMP_1]: SKIER_JUMP_1,
    [SKIER_DIRECTIONS.JUMP_2]: SKIER_JUMP_2,
    [SKIER_DIRECTIONS.JUMP_3]: SKIER_JUMP_3,
    [SKIER_DIRECTIONS.JUMP_4]: SKIER_JUMP_4
};

export const KEYS = {
    LEFT: 37,
    RIGHT: 39,
    UP: 38,
    DOWN: 40,
    JUMP: 32
};


export const OBSTACLE_ASSET = {
    [JUMP_RAMP]: {
        skierCanJump: true,
        skierCanSkip: true
    },
    [ROCK1]: {
        skierCanJump: false,
        skierCanSkip: true
    },
    [ROCK2]: {
        skierCanJump: false,
        skierCanSkip: true
    },
    [TREE]: {
        skierCanJump: false,
        skierCanSkip: false
    },
    [TREE_CLUSTER]: {
        skierCanJump: false,
        skierCanSkip: false
    }
};
