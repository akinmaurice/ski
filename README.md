# Ceros Ski Code Challenge

Welcome to the Ceros Code Challenge - Ski Edition!

For this challenge, we have included some base code for Ceros Ski, our version of the classic Windows game SkiFree. If
you've never heard of SkiFree, Google has plenty of examples. Better yet, you can play our version here:
http://ceros-ski.herokuapp.com/

Or deploy it locally by running:
```
npm install
npm run dev
```

There is no exact time limit on this challenge and we understand that everyone has varying levels of free time. We'd
rather you take the time and produce a solution up to your ability than rush and turn in a suboptimal challenge. Please
look through the requirements below and let us know when you will have something for us to look at. If anything is
unclear, don't hesitate to reach out.

**Solution**
* **Bug Fix:**
  When the Skier crashes and the left button is pressed,
  the direction asset for the skier is set to null.
  This makes the skier bound width value to be undefined causing the blank screen.
  To fix this, a check is done to determine if the direction asset is available before updating the direction.
  Also, When setting the direction to left in the turnLeft Method, the default crash direction value is 0,
  turning left removes 1 from 0 setting the new direction value to -1 which isn't available in the skier directions variable.
  A check is done to validate the value if it exists. if it doesn't the skier asset direction is set to left.

* **Extend Functionality:**
  The Skier jumps when he hits a ramp obstacle.
  The Skier can also jump when the space bar is pressed.
  This updates the skier asset to the jump asset and a skierIsJumping variable is set to true.
  When the skierIsJumping variable is true, Rock Obstacles can be skipped over by the skier.
  Tree and Tree Cluster Obstacles cannot be skipped over.
  A Obstacle Asset object was added to the constant file to handle this.
  In the future, if more obstacles are added,
  adding the skip and jump properties to the object ensures the collision method does not need to be updated
  before the skier can jump or skip over them.
  The Skier can also only jump for 1.2 seconds,
  after which the asset is set back to the skier down asset.


* **Build something new:**
  To Add the Rhino asset to the game, i made use of the existing entity class to create a new rhino class.
  The rhino starts moving when the game starts but from a configurable distance away from the skier.
  It also moves in whatever direction the skier moves in. When the skier moves diagonally, the speed reduces
  for the skier, it doesn't reduce for the rhino and the distance between them gets closer during this period.
  This gives the chance for the rhino to catch up with the skier.
  When the rhino catches up with the skier the game ends and restarts from the beginning again by reloading the game screen



**Bonus**
To Reset the game when its over, the Window is reloaded automatically when the Rhino catches the skier.
To Increase the speed of the game, when the skier hits 1000 on the y (vertical) axis,
the game starts to speed up by 0.0001 of the default speed.
To Implement the Score, Once the Rhino Catches the skier,
the Current Point on the vertical axis is divided by 10 to determine the score for the game session.
Added eslint config to ensure a constant style across
To run the lint script,
```
npm install -g eslint
npm run lint
```

Play the Updated version [Here](http://206.189.121.163/)


**Requirements**

* **Fix a bug:**

  There is a bug in the game. Well, at least one bug that we know of. Use the following bug report to debug the code
  and fix it.
  * Steps to Reproduce:
    1. Load the game
    2. Crash into an obstacle
    3. Press the left arrow key
  * Expected Result: The skier gets up and is facing to the left
  * Actual Result: Giant blizzard occurs causing the screen to turn completely white (or maybe the game just crashes!)

* **Write unit tests:**

  The base code has Jest, a unit testing framework, installed. Write some unit tests to ensure that the above mentioned
  bug does not come back.

* **Extend existing functionality:**

  We want to see your ability to extend upon a part of the game that already exists. Add in the ability for the skier to
  jump. The asset file for jumps is already included. All you gotta do is make the guy jump. We even included some jump
  trick assets if you wanted to get really fancy!
  * Have the skier jump by either pressing a key or use the ramp asset to have the skier jump whenever he hits a ramp.
  * The skier should be able to jump over some obstacles while in the air.
    * Rocks can be jumped over
    * Trees can NOT be jumped over
  * Anything else you'd like to add to the skier's jumping ability, go for it!

* **Build something new:**

  Now it's time to add something completely new. In the original Ski Free game, if you skied for too long,
  a yeti would chase you down and eat you. In Ceros Ski, we've provided assets for a Rhino to run after the skier,
  catch him and eat him.
  * The Rhino should appear after a set amount of time or distance skied and chase the skier, using the running assets
    we've provided to animate the rhino.
  * If the rhino catches the skier, it's game over and the rhino should eat the skier.

* **Documentation:**

  * Update this README file with your comments about your work; what was done, what wasn't, features added & known bugs.
  * Provide a way for us to view the completed code and run it, either locally or through a cloud provider

* **Be original:**
  * This should go without saying but don’t copy someone else’s game implementation!

**Grading**

Your challenge will be graded based upon the following:

* How well you've followed the instructions. Did you do everything we said you should do?
* The quality of your code. We have a high standard for code quality and we expect all code to be up to production
  quality before it gets to code review. Is it clean, maintainable, unit-testable, and scalable?
* The design of your solution and your ability to solve complex problems through simple and easy to read solutions.
* The effectiveness of your unit tests. Your tests should properly cover the code and methods being tested.
* How well you document your solution. We want to know what you did and why you did it.

**Bonus**

*Note: You won’t be marked down for excluding any of this, it’s purely bonus.  If you’re really up against the clock,
make sure you complete all of the listed requirements and to focus on writing clean, well organized, and well documented
code before taking on any of the bonus.*

If you're having fun with this, feel free to add more to it. Here's some ideas or come up with your own. We love seeing
how creative candidates get with this.

* Provide a way to reset the game once it's over
* Provide a way to pause and resume the game
* Add a score that increments as the skier skis further
* Increase the difficulty the longer the skier skis (increase speed, increase obstacle frequency, etc.)
* Deploy the game to a server so that we can play it without having to install it locally
* Write more unit tests for your code

We are looking forward to see what you come up with!
